#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include "shell.h"
#include "helper.h"
#include "command.h"
#include <signal.h>
#include <errno.h>
#include <pwd.h>
#include <sys/types.h>
#include <fcntl.h>
#include "statuslist.h"
#include "debug.h"
#include "execute.h"

/* do not modify this */
#ifndef NOLIBREADLINE

#include <readline/history.h>

#endif /* NOLIBREADLINE */
#define INPUT  0
#define OUTPUT  1

extern int shell_pid;
extern int fdtty;
int interruptFlag = 0;

ProcessList * pl;
/* do not modify this */
#ifndef NOLIBREADLINE

static int builtin_hist(char **command) {

    register HIST_ENTRY **the_list;
    register int i;
    printf("--- History --- \n");

    the_list = history_list();
    if (the_list)
        for (i = 0; the_list[i]; i++)
            printf("%d: %s\n", i + history_base, the_list[i]->line);
    else {
        printf("history could not be found!\n");
    }

    printf("--------------- \n");
    return 0;
}

#endif /*NOLIBREADLINE*/

void unquote(char *s) {
    if (s != NULL) {
        if (s[0] == '"' && s[strlen(s) - 1] == '"') {
            char *buffer = calloc(sizeof(char), strlen(s) + 1);
            strcpy(buffer, s);
            strncpy(s, buffer + 1, strlen(buffer) - 2);
            s[strlen(s) - 2] = '\0';
            free(buffer);
        }
    }
}

void unquote_command_tokens(char **tokens) {
    int i = 0;
    while (tokens[i] != NULL) {
        unquote(tokens[i]);
        i++;
    }
}

void unquote_redirect_filenames(List *redirections) {
    List *lst = redirections;
    while (lst != NULL) {
        Redirection *redirection = (Redirection *) lst->head;
        if (redirection->r_type == R_FILE) {
            unquote(redirection->u.r_file);
        }
        lst = lst->tail;
    }
}

void unquote_command(Command *cmd) {
    List *lst = NULL;
    switch (cmd->command_type) {
        case C_SIMPLE:
        case C_OR:
        case C_AND:
        case C_PIPE:
        case C_SEQUENCE:
            lst = cmd->command_sequence->command_list;
            while (lst != NULL) {
                SimpleCommand *cmd_s = (SimpleCommand *) lst->head;
                unquote_command_tokens(cmd_s->command_tokens);
                unquote_redirect_filenames(cmd_s->redirections);
                lst = lst->tail;
            }
            break;
        case C_EMPTY:
        default:
            break;
    }
}



static int execute_fork(SimpleCommand *cmd_s, int background) {
    char **command = cmd_s->command_tokens;
    int fds;
    pid_t pid, wpid;
    Process *process = newProcess(-1, -1, cmd_s->command_tokens);
    pl = appendProcess(process, pl);
    pid = fork();
    process->pid = pid;
    if (pid == 0) {
        /* child */
        signal(SIGINT, SIG_DFL);
        signal(SIGTTOU, SIG_DFL);
        /*
         * handle redirections here
         */
        if (cmd_s->redirections != NULL) {
            List *rList = cmd_s->redirections;
            while (rList != NULL) {
                Redirection *rd = (Redirection *) rList->head;
                switch (rd->r_mode) {
                    case M_READ:
                        if (rd->u.r_file != NULL) {
                            if((fds = open(rd->u.r_file, O_RDONLY)) == -1){
                                perror(rd->u.r_file);
                            }
                        }

                        if (dup2(fds, STDIN_FILENO) < 0) {
                            printf("Unable to duplicate file descriptor.\n");
                            exit(EXIT_FAILURE);
                        }
                        rList = rList->tail;
                        break;
                    case M_APPEND:
                        if (rd->u.r_file != NULL) {
                            if((fds = open(rd->u.r_file, O_WRONLY | O_CREAT | O_APPEND, 00644)) == -1){
                                perror(rd->u.r_file);
                            }
                        }
                        if (dup2(fds, STDOUT_FILENO) < 0) {
                            printf("Unable to duplicate file descriptor.\n");
                            exit(EXIT_FAILURE);
                        }
                        rList = rList->tail;
                        break;
                    case M_WRITE:
                        if (rd->u.r_file != NULL) {
                            if((fds = open(rd->u.r_file, O_WRONLY | O_CREAT | O_TRUNC, 00644)) == -1){
                                perror(rd->u.r_file);
                            }
                        }
                        if (dup2(fds, STDOUT_FILENO) < 0) {
                            printf("Unable to duplicate file descriptor.\n");
                            exit(EXIT_FAILURE);
                        }
                        rList = rList->tail;
                        break;
                    default:
                        rList = rList->tail;
                        break;
                }
            }
        }
        if (execvp(command[0], command) == -1) {
            fprintf(stderr, "-bshell: %s : command not found \n", command[0]);
            perror("");
        }
        /*exec only return on error*/
        exit(EXIT_FAILURE);
    } else if (pid < 0) {
        perror("shell");

    } else {
        /*parent*/
        setpgid(pid, pid);
        process->pgid = pid;
        if (background == 0) {
            int status;
            /* wait only if no background process */
            tcsetpgrp(fdtty, pid);
            wpid = waitpid(pid, &status, 0);
            process->isRunning = 0;
            process->status = status;

            tcsetpgrp(fdtty, shell_pid);
            return status;
        }

    }

    return 0;
}




static int executePipe(Command *cmd, int background) {
    List *lst = cmd->command_sequence->command_list;
    int pids[cmd->command_sequence->command_list_len];
    int pidCounter =0;
    int counter = 0;
    pid_t pid, wpid;
    int pfd[cmd->command_sequence->command_list_len - 1][2]; // Pipe file descriptors
    for(int i = 0; i<  cmd->command_sequence->command_list_len-1; i++){
        if (pipe2(pfd[i], O_CLOEXEC) == -1) { // Create the pipe
            perror("Pipe failed: \n");
            exit(EXIT_FAILURE);
        }
    }
    while (lst != NULL) {
        SimpleCommand *sc = (SimpleCommand *) lst->head;
        char **command = sc->command_tokens;
        Process * process = newProcess(-1, -1, command);
        pl = appendProcess(process, pl);
        pid = fork();
        pids[pidCounter++] =pid;
        if (pid == 0) {// fork child
            if (counter == 0) {
                // fprintf(stderr, "redirect output\n");
                if (dup2(pfd[counter][OUTPUT], STDOUT_FILENO) < 0) {
                    perror("dup failed\n");
                    exit(EXIT_FAILURE);
                }
                //fprintf(stderr, "redirected start\n");
            } else if (counter > 0 && counter < cmd->command_sequence->command_list_len - 1) {
                //fprintf(stderr, "redrect input\n");
                if (dup2(pfd[counter-1][INPUT], STDIN_FILENO) < 0) {
                    perror("dup failed\n");
                    exit(EXIT_FAILURE);
                }
                //fprintf(stderr, "rediret output\n");
                if (dup2(pfd[counter][OUTPUT], STDOUT_FILENO) < 0) {
                    perror("dup failed\n");
                    exit(EXIT_FAILURE);
                }
                //fprintf(stderr, "redirected mitte\n");
            } else if (counter == cmd->command_sequence->command_list_len - 1) {
                //fprintf(stderr, "rediret input\n");
                if (dup2(pfd[counter - 1][INPUT], STDIN_FILENO) < 0) {
                    perror("dup failed\n");
                    exit(EXIT_FAILURE);
                }
                //fprintf(stderr, "redirected end\n");
            }
            //fprintf(stderr, "counter: %d\n", counter);
            //fprintf(stderr, "check1\n");
            /*if(counter > 0){
                close(pfd[counter-1][OUTPUT]);
                close(pfd[counter-1][INPUT]);
            }*/
            setpgid(0, pids[0]);
            if (background == 0) {
                tcsetpgrp(fdtty, getpgid(0));
            }
            signal(SIGINT, SIG_DFL);
            signal(SIGTTOU, SIG_DFL);
            if (execvp(command[0], command) == -1) {
                fprintf(stderr, "-bshell: %s : command not found \n", command[0]);
                perror("");
            }
            exit(EXIT_FAILURE);
        } else if (pid < 0) {
            perror("shell");

        }else{
            process->pgid = pids[0];
            process->pid = pid;
            counter++;
            lst = lst->tail;
        }
    }
    for(int i = 0; i<  cmd->command_sequence->command_list_len-1; i++){
        close(pfd[i][0]);
        close(pfd[i][1]);
    }
    //parent
    setpgid(pids[0], pids[0]);
    if (background == 0) {
        int status;
        /* wait only if no background process */
        tcsetpgrp(fdtty, pids[0]);

        //Überarbeiten
        for(int i =0; i< cmd->command_sequence->command_list_len; i++){
            wpid = waitpid(pids[i], &status, 0);
            updateList(wpid, status);
        }
        tcsetpgrp(fdtty, shell_pid);
        return status;
    }


    return 0;
}


static int do_execute_simple(SimpleCommand *cmd_s, int background) {
    if (cmd_s == NULL) {
        return 0;
    }

    if (strcmp(cmd_s->command_tokens[0], "exit") == 0) {
        if (cmd_s->command_tokens[1] == NULL) {
            exit(0);
        } else {
            char *c;
            int ll = strtol(cmd_s->command_tokens[1], &c, 10);
            exit(ll);
        }

/* do not modify this */
#ifndef NOLIBREADLINE
    } else if (strcmp(cmd_s->command_tokens[0], "hist") == 0) {
        return builtin_hist(cmd_s->command_tokens);
#endif /* NOLIBREADLINE */
    } else if (strcmp(cmd_s->command_tokens[0], "cd") == 0) {
        const char *dir;
        if (cmd_s->command_tokens[1] == NULL) {
            dir = getenv("HOME");
            if(dir == NULL){
                fprintf(stderr, "HOME not set\n");
                return 1;
            }
        }else{
            dir = cmd_s->command_tokens[1];
        }
        int out = chdir(dir);
        if (out != 0) {
            perror(dir);
        }
        return out;
    }else if(strcmp(cmd_s->command_tokens[0], "status") == 0){
        printProcessList(pl);
        pl = deleteProcess(pl);
        return 0;
    }else {
        return execute_fork(cmd_s, background);
    }
    fprintf(stderr, "This should never happen!\n");
    exit(1);
}

/*
 * check if the command is to be executed in back- or foreground.
 *
 * For sequences, the '&' sign of the last command in the
 * sequence is checked.
 *
 * returns:
 *      0 in case of foreground execution
 *      1 in case of background execution
 *
 */
int check_background_execution(Command *cmd) {
    List *lst = NULL;
    int background = 0;
    switch (cmd->command_type) {
        case C_SIMPLE:
            lst = cmd->command_sequence->command_list;
            background = ((SimpleCommand *) lst->head)->background;
            break;
        case C_OR:
        case C_AND:
        case C_PIPE:
        case C_SEQUENCE:
            /*
             * last command in sequence defines whether background or
             * forground execution is specified.
             */
            lst = cmd->command_sequence->command_list;
            while (lst != NULL) {
                background = ((SimpleCommand *) lst->head)->background;
                lst = lst->tail;
            }
            break;
        case C_EMPTY:
        default:
            break;
    }
    return background;
}


int execute(Command *cmd) {
    unquote_command(cmd);

    int res = 0;
    List *lst = NULL;

    int execute_in_background = check_background_execution(cmd);
    switch (cmd->command_type) {
        case C_EMPTY:
            break;
        case C_SIMPLE:
            res = do_execute_simple((SimpleCommand *) cmd->command_sequence->command_list->head, execute_in_background);
            fflush(stderr);
            break;

        case C_OR:
            lst = cmd->command_sequence->command_list;
            while (lst != NULL) {
                res = do_execute_simple((SimpleCommand *) lst->head, execute_in_background);
                fflush(stderr);
                if (res == 0) {
                    break;
                } else {
                    lst = lst->tail;
                }
            }
            break;
        case C_AND:
            lst = cmd->command_sequence->command_list;
            while (lst != NULL) {
                res = do_execute_simple((SimpleCommand *) lst->head, execute_in_background);
                fflush(stderr);
                if (res == 0) {
                    lst = lst->tail;
                } else {
                    break;
                }
            }
            break;
        case C_SEQUENCE:
            lst = cmd->command_sequence->command_list;
            while (lst != NULL) {
                res = do_execute_simple((SimpleCommand *) lst->head, execute_in_background);
                fflush(stderr);
                lst = lst->tail;
            }
            break;
        case C_PIPE:
            executePipe(cmd, execute_in_background);
            break;
        default:
            printf("[%s] unhandled command type [%i]\n", __func__, cmd->command_type);
            break;
    }
    return res;
}

