#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "statuslist.h"

extern ProcessList * pl;

Process * newProcess(pid_t pid, pid_t pgid, char ** cmd){
    Process * pc = malloc(sizeof(Process));
    if(pc == NULL){
        perror("malloc failed");
        exit(EXIT_FAILURE);
    }
    pc->pid = pid;
    pc->pgid = pgid;
    pc->cmd = malloc(strlen(*cmd) + 1);
    if(pc->cmd == NULL){
        perror("malloc failed");
        exit(EXIT_FAILURE);
    }
    strcpy(pc->cmd, *cmd);
    pc->status = 0;
    pc->isRunning = 1;
    pc->isShown = 0;
    return pc;
}

ProcessList * appendProcess(Process * process, ProcessList * processList){
    ProcessList * list = malloc(sizeof(Process));
    if(list == NULL){
        perror("malloc failed\n");
        exit(EXIT_FAILURE);
    }
    list->head = process;
    list->next = processList;
    list->previous = NULL;

    if(processList != NULL){
        processList->previous = list;
    }
    return list;
}

Process * findPid(pid_t pid){
    ProcessList * processList = pl;
    while(processList != NULL){
        Process * p = processList->head;
        if(p->pid == pid){
            return p;
        }
        processList = processList->next;
    }
    return NULL;
}

ProcessList *deleteProcess(ProcessList * processList){
    ProcessList *newProcessList = processList;
    ProcessList * previous = NULL;
    ProcessList * next = NULL;
    while(processList != NULL) {
        Process *p = processList->head;
        if (p->isShown) {
            free(p->cmd);
            free(p);
            next = processList->next;
            previous = processList->previous;

            if (previous == NULL) {
                newProcessList = next;
            } else {
                previous->next = next;
            }
            if (next != NULL) {
                next->previous = previous;
            }
        }
        processList = processList->next;
    }

    pl = newProcessList;
    return newProcessList;
}

void printProcessList(ProcessList * processList){
    printf("PID\tPGID\tSTATUS\tPROG\n");
    while(processList != NULL){
        Process * p = processList->head;
        if(p->isRunning == 0){
            p->isShown = 1;
        }
        if(p->isRunning){
            printf("%d\t%d\t%s\t%s\n", p->pid, p->pgid, "running", p->cmd);

        }else if(WIFEXITED(p->status)){
            printf("%d\t%d\texit(%d)\t%s\n", p->pid, p->pgid, WEXITSTATUS(p->status), p->cmd);
        }else if(WIFSIGNALED(p->status)){
            printf("%d\t%d\tsignal(%d)\t%s\n", p->pid, p->pgid, WTERMSIG(p->status), p->cmd);

        }else{
            printf("%d\t%d\t%d\t%s\n", p->pid, p->pgid, p->status, p->cmd);
        }
        processList = processList->next;
    }
}

void updateList(pid_t pid, int status){
    ProcessList * list = pl;
    while(list != NULL){
        if(list->head->pid == pid){
            list->head->status = status;
            list->head->isRunning = 0;
        }else if(list->head->pid == -1){
            list->head->pid = pid;
            list->head->status = status;
            list->head->isRunning = 0;
        }
        list = list->next;
    }
}




