#ifndef STATUSLIST_H

#define STATUSLIST_H

typedef struct {
    pid_t pid;
    pid_t pgid;
    int isRunning;
    int status;
    int isShown;
    char * cmd;
}Process;

typedef struct processList {
    Process *head;
    struct processList *next;
    struct processList *previous;
} ProcessList;


Process * newProcess(pid_t pid, pid_t pgid, char ** cmd);
ProcessList * appendProcess(Process * process, ProcessList * processList);
Process * findPid(pid_t pid);
ProcessList *deleteProcess(ProcessList * processList);
void printProcessList(ProcessList * processList);
void updateList(pid_t pid, int status);



#endif /* end of include guard: STATUSLIST_H */
